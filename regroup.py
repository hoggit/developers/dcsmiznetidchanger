from __future__ import print_function

import pickle
import os
import sys
import tempfile
import zipfile
import zlib
import lupa
from lupa import LuaRuntime
lua = LuaRuntime(unpack_returned_tuples=True)

groupIdArray = []
assignedIdArray = []

def main(input_path):

    #unpack the given input .miz file
    mizfile = zipfile.ZipFile(input_path,mode='r')
    for filename in mizfile.namelist():
        mizfile.extract(filename,".\data")


    with open(".\data\mission", encoding="utf-8") as handle:
        content = handle.read()
    lua.execute(content)

    #invert content string
    revContent = content[::-1]
    #convert content to list to make it editable
    revContentList = list(revContent)

    used_group_ids = set()
    hog_groups = []

    for country_index, country in lua.globals().mission.coalition.blue.country.items():
        if not country.plane:
            continue
        for groupIndex, group in country["plane"]["group"].items():
            try:
                eplrs = group.route.points[1].task.params.tasks[2].params.action
            except AttributeError:
                continue
            if not eplrs or eplrs.id != "EPLRS":
                continue
            hog_group = group.units[1].type == "A-10C"
            if hog_group and group.units[1].skill == "Client":
                hog_groups.append(group)
            else:
                used_group_ids.add(eplrs.params.groupId)

    available_group_ids = sorted(set(range(1, 100)) - used_group_ids)
    if len(hog_groups) > len(available_group_ids):
        raise ValueError("this won't be good, not enough group ids in range")


    for hog_group, available_id in zip(hog_groups, available_group_ids):
        hog_group.route.points[1].task.params.tasks[2].params.action.params.groupId = available_id
        print("Will assign hog map group", hog_group.groupId, "to", available_id)
        groupIdArray.append(hog_group.groupId)
        assignedIdArray.append(available_id)

    for i in range(0,len(groupIdArray),1):
        
        
        #search string for groupID index and netID index
        groupStringPos = revContent.find(('["groupId"] = ' + str(groupIdArray[i]))[::-1])
        #search string for NetId and return index
        netStringPos = revContent.find(('["groupId"] = ')[::-1],(groupStringPos+20))

        #delete digits from old NetID
        for j in range((netStringPos-1),(netStringPos-10),-1):
            
            if str(revContentList[j]).isdigit():

                del revContentList[j]
                
       #insert didgits for new NetId         
        for k in reversed(range(0,len(assignedIdArray),1)):

            if k == i:
                Id = str(assignedIdArray[i])
                invId = Id[::-1]
                revContentList.insert((netStringPos-1),invId)

    #formatting for export (reversing, converting to string)
    invExport = "".join(revContentList)
    export = invExport[::-1]
    #writing the new mission file
    exportHandle = open("mission","w+",encoding="utf-8")
    exportHandle.write(export)
    #create the export directory for the final version of the .miz
    try:
        os.mkdir(".\export")
    except:
        print("Folder \"export\" already exists!")

    exportPath = os.path.normpath("./export/"+input_path)
    #create a new .miz file from the original content and the new mission file in the export folder
    exportMiz = zipfile.ZipFile(exportPath,mode="a",compression=zipfile.ZIP_DEFLATED)
    for (root,dirs,files) in os.walk(".\data"):
        for file in os.listdir(root):
            if os.path.isfile(root+"\\"+file) and file != "mission":
                currentfile = (root+"\\"+file)
                exportMiz.write(currentfile,"."+root[6:]+"\\"+file)
    exportMiz.write("mission")
    exportMiz.close()
   

            
if __name__ == "__main__":            
    if len(sys.argv) < 2:
        print("Usage: %s mission_file" % sys.argv[0])
        sys.exit(1)
    sys.exit(main(os.path.abspath(sys.argv[1])))