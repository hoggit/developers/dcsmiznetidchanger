# DcsMizNetIDChanger

This script sets each A-10C in a given mission to a unique SADL NetID.

It takes a dcs.miz file, unpacks it to the folder .\data, does its thing, and then rezips the mission to the .\export folder.